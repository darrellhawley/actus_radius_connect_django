<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>W Atlanta Free Wi-Fi Coming Soon</title>

<link rel="icon" type="image/ico" href="http://www.watlantadowntown.com/favicon.ico" />

<meta name="apple-mobile-web-app-capable" content="yes" />

<script type='text/javascript' src="/radiusconnect/actus/js/jquery.min.js"></script>
<script type='text/javascript' src='js/infinite-rotator.js'></script>

<script type='text/javascript'>
// When ready...
window.addEventListener("load",function() {
  // Set a timeout...
  setTimeout(function(){
    // Hide the address bar!
    window.scrollTo(0, 1);
  }, 0);
});

</script>
        
<style type="text/css">
body {
	background-color: #000;
	margin: 0px;
	padding: 0px;
}
html, body
{
  height: 100%;
}
a:active {
  outline: none;
}
.table_top {
	margin: 0px;
	padding: 0px;
	height: 400px;
	background-image: url(images/bodyback_top.jpg);
	background-repeat: repeat-x;
	background-position: center top;
	
}
.table_mid {
	margin: 0px;
	padding: 0px;
	height: 68px;
	background-image: url(images/bodyback_mid.jpg);
	background-repeat: repeat-x;
	background-position: center top;
	
}
#holder_top {
	padding: 0px;
	height: 400px;
	width: 948px;
	margin-right: auto;
	margin-left: auto;
	text-align: left;
}
#holder_mid {
	padding: 0px;
	height: 68px;
	width: 948px;
	margin-right: auto;
	margin-left: auto;
	text-align: left;
}
form label {
	font-family: Arial, Helvetica, sans-serif;
	color: #999;
	font-size: 14px;
	line-height: 14px;
	font-weight: bold;
	padding: 0px;
	margin-top: -3px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
}
form input {
	font-family: Arial, Helvetica, sans-serif;
	color: #FFFFFF;
	font-size: 13px;
	height: 25px;
	margin-top: 0px;
	margin-right: 20px;
	margin-bottom: 0px;
	margin-left: 10px;
	line-height: 12px;
	padding-top: 0px;
	padding-right: 3px;
	padding-bottom: 0px;
	padding-left: 3px;
	background-color: #6C6C6C;
	border: 1px solid #999999;
	box-shadow: 4px 4px 7px rgba(0,0,0,0.4);
	width: 150px;
}
form input.button {
	font-family: Arial, Helvetica, sans-serif;
	color: #4F4F4F;
	font-size: 13px;
	height: 25px;
	margin-top: 0px;
	margin-right: 20px;
	margin-bottom: 0px;
	margin-left: 20px;
	line-height: 12px;
	background-color: #999999;
	border: 1px solid #999999;
	font-weight: bold;
	box-shadow: 4px 4px 7px rgba(0,0,0,0.4);
	padding: 3px;
	width: 100px;
}
form input.button:hover {
	font-family: Arial, Helvetica, sans-serif;
	color: #FFFFFF;
	font-size: 13px;
	height: 25px;
	margin-top: 0px;
	margin-right: 20px;
	margin-bottom: 0px;
	margin-left: 20px;
	line-height: 12px;
	padding-top: 3px;
	padding-right: 3px;
	padding-bottom: 3px;
	padding-left: 3px;
	background-color: #999999;
	border: 1px solid #999999;
	font-weight: bold;
	box-shadow: 4px 4px 7px rgba(0,0,0,0.4);
}
form {
	height: 30px;
	margin-top: 20px;
	margin-left: 285px;
	padding: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
#holder_mid p {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	line-height: 14px;
	color: #D9D9D9;
	padding: 0px;
	margin-top: 28px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 300px;
}
#holder_ftr {
	padding: 12px;
	width: 918px;
	margin-right: auto;
	margin-left: auto;
	text-align: left;
	margin-top: 0px;
	margin-bottom: 0px;
}
#holder_ftr p{
	font-family: Arial, Helvetica, sans-serif;
	color: #686868;
	font-size: 11px;
	margin: 0px;
	padding-top: 0px;
	padding-right: 0px;
	padding-bottom: 7px;
	padding-left: 0px;
	line-height: 12px;
}
#holder_ftr p a {
	color: #686868;
	text-decoration: none;
}
#holder_ftr p a:hover {
	color: #CCCCCC;
	text-decoration: none;
}
#rotating-item-wrapper {
	position: relative;
	width: 948px;
	height: 170px;
	margin: 0px;
	padding: 0px;
	float: left;
	background-image: url(images/animate_back.jpg);
}
.rotating-item {
    display: none;
    position: absolute;
    top: 0;
    left: 0;
}


</style>



</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height: 100%;">
  <tr>
    <td align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="top" class="table_top">
        
        <div id="holder_top">
        
        <div id="rotating-item-wrapper">
	<img src="images/01.jpg" class="rotating-item" width="948" height="170" />
    <img src="images/02.jpg" class="rotating-item" width="948" height="170" />
    <img src="images/03.jpg" class="rotating-item" width="948" height="170" />
			
		</div>
        
        <img src="images/welcome.jpg" width="948" height="230" border="0" usemap="#Map" /></div>
        
        </td>
      </tr>
      <tr>
        <td align="center" valign="top" class="table_mid">
        
       
        <!--
        <div id="holder_mid">
           
          
           <?php
   if ($_SERVER['REQUEST_METHOD'] != 'POST'){
      $me = $_SERVER['PHP_SELF'];
?>
    
    
    <form name="form1" method="post"
         action="<?php echo $me;?>">
            <label for="name">Name<input type="text" name="name" id="name" /></label><label for="email">Email<input type="text" name="email" id="email" /></label>
            <input name="button" type="submit" class="button" id="button" value="Register Me!" />
    </form>
    
    <?php
} else {
      error_reporting(0);

      $errors = array();

      $page = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
      
	   if (count($errors)>0) {
		  echo "<p class=\"oops\">Oops! There was a problem:</p>";
         foreach($errors as $err)
		    echo "$err<br>\n";
         echo "<p class=\"goback\">Please use your browser's Back button to fix.</p>";
   } else {

      error_reporting(0);
      $recipient = 'iricelis.patino@gmail.com';
	  $subject = 'W FREE WI-FI UPDATE SIGNUP';
      $name = stripslashes($_POST['name']);
      $email = stripslashes($_POST['email']);
	  $from = "$email"; 
      
	  $msg = "The following person filled out the W Free Wi-Fi Updates online form:

Name: $name
Email: $email

";



if (mail($recipient, $subject, $msg, $from))
         echo nl2br("<p>Thank you $name for registering for updates</p>
");
      else
         echo "Message failed to send";
}

 
}
?>

         
		  
        </div>
        -->
        </td>
      </tr>
      <tr>
        <td align="center" valign="top">
        
        <div id="holder_ftr">
        
        <p>More Starwood Hotels:</span> <a href="http://www.watlantabuckhead.com/" target="_blank">W Atlanta - Buckhead</a> | <a href="http://www.watlantamidtown.com/" target="_blank">W Atlanta - Midtown</a> | <a href="http://www.watlantaperimeter.com/" target="_blank">Atlanta Perimeter Hotel & Suites</a>		</p>
        
       <p> <a href="http://www.starwoodhotels.com/bestrate/index.html" target="_blank">Best Rate Guarantee</a>
                    | <a href="http://www.starwoodhotels.com/whotels/explore/world/hotels.html" target="_blank">W Hotel Directory</a>
                                        | <a target="_parent" href="http://www.watlantadowntown.com/terms" >Website Terms of Use</a>
                                        										| <a href="http://www.starwoodhotels.com/whotels/property/features/accessibility.html?propertyID=1788&language=en_US&localCode=en_US" target="_blank">Accessibility Information</a></p>
              <p><a href="http://www.starwoodhotels.com/index.html" target="_blank">&copy; 2009-2012 45 Allen Plaza Associates LLC with portions of website content &copy; 2009-2012  Starwood Hotels &amp; Resorts Worldwide Inc.</a> All rights reserved. </p>
              <p><img src="images/spg.jpg" width="833" height="77" border="0" usemap="#Map2" /></p>  
        
        </div>
        
        </td>
      </tr>
    </table></td>
  </tr>
</table>

<map name="Map2" id="Map2">
  <area shape="rect" coords="0,12,125,72" href="http://www.starwoodhotels.com/preferredguest/index.html" target="_blank" />
  <area shape="rect" coords="365,5,438,30" href="http://www.starwoodhotels.com/lemeridien/index.html" target="_blank" />
  <area shape="rect" coords="411,37,447,77" href="http://www.starwoodhotels.com/whotels/index.html" target="_blank" />
  <area shape="rect" coords="460,3,514,29" href="http://www.starwoodhotels.com/alofthotels/index.html" target="_blank" />
  <area shape="rect" coords="490,36,547,77" href="http://www.starwoodhotels.com/sheraton/index.html" target="_blank" />
  <area shape="rect" coords="530,-8,588,29" href="http://www.starwoodhotels.com/fourpoints/index.html" target="_blank" />
  <area shape="rect" coords="573,37,640,78" href="http://www.starwoodhotels.com/stregis/index.html" target="_blank" />
  <area shape="rect" coords="603,2,666,30" href="http://www.starwoodhotels.com/westin/index.html" target="_blank" />
  <area shape="rect" coords="671,37,724,78" href="http://www.starwoodhotels.com/element/index.html" target="_blank" />
  <area shape="rect" coords="687,-2,763,30" href="http://www.starwoodhotels.com/luxury/index.html" target="_blank" />
</map>

<?php include '../radius.php'; ?>

</body>
</html>
